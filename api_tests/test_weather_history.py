from datetime import datetime, timedelta, timezone

import requests

from automation_project.api_client import WeatherApiClient


class TestWeatherHistory:
    """Test cases for weather history."""

    def setup_method(self):
        """Setup method to initialize common resources."""
        self.weather_api_client = WeatherApiClient()

    def test_weather_history_in_city_three_days_ago(self):
        """
        Test the weather history in the city 3 days ago.
        API Endpoint: http://api.weatherapi.com/v1/history.json?key=<API_KEY>&q=<CITY>&dt=<DATE>
        Should return:
            - Status code is 200
            - Valid JSON data with the name of the city and correct date
            - Temperature values within an expected range
        """
        url = f"{self.weather_api_client.base_url}/history.json"
        api_key = self.weather_api_client.api_key
        city = "aviv"

        utc_timezone = timezone.utc
        today_start_utc = datetime.utcnow().replace(
            hour=0, minute=0, second=0, microsecond=0, tzinfo=utc_timezone
        )
        three_days_ago_utc = today_start_utc - timedelta(days=3)

        formatted_date = three_days_ago_utc.strftime("%Y-%m-%d")
        params = {"key": api_key, "q": city, "dt": formatted_date}

        response = requests.get(url=url, params=params)

        assert response.status_code == 200, "Expected status code 200"

        json_data = response.json()

        actual_date_epoch = (
            json_data.get("forecast", {}).get("forecastday", [{}])[0].get("date_epoch")
        )
        expected_date_epoch = int(three_days_ago_utc.timestamp())

        assert actual_date_epoch == expected_date_epoch, "Unexpected date_epoch"

        day_data = (
            json_data.get("forecast", {}).get("forecastday", [{}])[0].get("day", {})
        )

        max_temp_c = day_data.get("maxtemp_c")
        min_temp_c = day_data.get("mintemp_c")
        avg_temp_c = day_data.get("avgtemp_c")

        expected_min_temp = -20
        expected_max_temp = 40

        assert (
            expected_min_temp <= min_temp_c <= expected_max_temp
        ), "Unexpected min temperature"
        assert (
            expected_min_temp <= avg_temp_c <= expected_max_temp
        ), "Unexpected avg temperature"
        assert (
            expected_min_temp <= max_temp_c <= expected_max_temp
        ), "Unexpected max temperature"
