import os

import pytest


def run_api_tests():
    print("Running API tests...")
    current_directory = os.path.dirname(os.path.realpath(__file__))
    api_tests_path = os.path.join(current_directory, "api_tests")
    pytest.main([api_tests_path])


def run_ui_tests():
    print("Running UI tests...")
    current_directory = os.path.dirname(os.path.realpath(__file__))
    ui_tests_path = os.path.join(current_directory, "ui_tests")
    pytest.main([ui_tests_path])


if __name__ == "__main__":
    run_api_tests()
    run_ui_tests()
    print("Tests execution complete!")
