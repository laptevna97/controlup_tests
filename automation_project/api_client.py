import os

from dotenv import load_dotenv


class WeatherApiClient:
    """Client for accessing weather API."""

    def __init__(self):
        """Initialize WeatherApiClient."""
        load_dotenv()
        self.base_url = os.getenv("API_BASE_URL")
        self.api_key = os.getenv("WEATHER_API_KEY")

        if not self.base_url or not self.api_key:
            raise ValueError(
                "BASE_URL and WEATHER_API_KEY must be provided in the environment."
            )
