import os

from dotenv import load_dotenv
from selenium import webdriver
from selenium.common import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class WeatherUIClient:
    """
    Wrapper for interacting with the weather UI.
    """

    def __init__(self):
        """
        Initialize the Weather UI Client.
        """
        self._initialize_driver()

        if not self.base_url or not self.api_key:
            raise ValueError(
                "BASE_URL and WEATHER_API_KEY must be provided in the environment."
            )

    def _initialize_driver(self):
        """
        Initialize the WebDriver.
        """
        load_dotenv()
        self.base_url = os.getenv("UI_BASE_URL")
        self.api_key = os.getenv("WEATHER_API_KEY")
        options = Options()
        chromedriver_path = ''                                      #Enter here your chromedriver path if you have problems with Selenium during running UI tests
        service = Service(executable_path=chromedriver_path)
        self.driver = webdriver.Chrome(options=options)             #Add service=service if you have porblems during running UI tests

    def check_url(self, url, timeout=10):
        """
        Check if the given URL is in the current browser URL.

        :param url: The URL to check.
        :param timeout: Maximum time to wait for the URL to be present.
        :return: True if the URL is present, False otherwise.
        """
        try:
            WebDriverWait(self.driver, timeout).until(EC.url_contains(url))
            return True
        except TimeoutException:
            return False

    def clear_field(self, locator):
        """
        Clear the text field.

        :param locator: The locator of the text field.
        """
        try:
            locator.clear()
        except NoSuchElementException:
            raise
        except Exception as ex:
            raise Exception(f"Error clearing field: {ex}")

    def send_keys(self, locator, text, errorMessage="Object not found"):
        """
        Send keys to the specified locator.

        :param locator: The locator to send keys.
        :param text: The text to send.
        :param errorMessage: Custom error message.
        :return: The locator.
        """
        try:
            self.clear_field(locator)
            locator.send_keys(text)
            return locator
        except NoSuchElementException:
            raise
        except Exception as ex:
            message = (
                f"{errorMessage}: {ex}"
                if errorMessage != "Object not found"
                else str(ex)
            )
            raise Exception(message)

    def quit_browser(self):
        """
        Quit the browser.
        """
        self.driver.quit()


