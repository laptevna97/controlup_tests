class WeatherSearchConstants:
    """
    Constants for weather search UI elements.
    """

    WAIT_TIMEOUT = 10
    API_KEY_INPUT = '//*[@id="ctl00_MainContentHolder_txtAPIKey"]'
    PROTOCOL_INPUT = '//*[@id="ctl00_MainContentHolder_cmbProtocol"]'
    HTTP_PROTOCOL = '//*[@id="ctl00_MainContentHolder_cmbProtocol"]/option[1]'
    FORMAT_INPUT = '//*[@id="ctl00_MainContentHolder_cmbFormat"]'
    JSON_FORMAT = '//*[@id="ctl00_MainContentHolder_cmbFormat"]/option[1]'
    VALUE_INPUT = '//*[@id="ctl00_MainContentHolder_txtQ"]'

    SHOW_RESPONSE_BUTTON = '//*[@id="search"]//button'

    RESULT_CHART = '//*[@id="resultae"]'
    COUNTRY_KEY = f'//*[@id="resultae"]//span[text()=\'"country"\']'
