import os

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from automation_project.ui_client import WeatherUIClient
from ui_tests.constants.weather_search_constants import WeatherSearchConstants


class TestWeatherSearchForCity(WeatherSearchConstants):
    """
    Test cases for weather search functionality.
    """

    def setup_method(self):
        """
        Setup method to initialize common resources.
        """
        self.weather_ui_client = WeatherUIClient()

    def test_weather_search_for_city(self):
        """
        Test the weather search for a city.
        """
        url = f"{self.weather_ui_client.base_url}#search"
        api_key = self.weather_ui_client.api_key
        city_for_search = os.getenv("CITY_FOR_UI_AUTOMATION")
        country_to_check = os.getenv("COUNTRY_FOR_UI_AUTOMATION")
        weather_search_url = "https://www.weatherapi.com/api-explorer.aspx#search"
        driver = self.weather_ui_client.driver
        driver.get(url)

        wait = WebDriverWait(driver, self.WAIT_TIMEOUT)

        assert self.weather_ui_client.check_url(
            weather_search_url
        ), f"{weather_search_url} is not in the URL"

        api_key_input = driver.find_element(By.XPATH, self.API_KEY_INPUT)
        assert api_key_input, f"API_KEY_INPUT does not exist"

        assert self.weather_ui_client.send_keys(
            api_key_input, api_key
        ), f"{api_key} is not writing in API_KEY_INPUT "

        protokol_input = driver.find_element(By.XPATH, self.PROTOCOL_INPUT)
        assert protokol_input, f"PROTOCOL_INPUT does not exist"

        protokol_input.click()

        http_protocol_option = driver.find_element(By.XPATH, self.HTTP_PROTOCOL)
        assert http_protocol_option, f"HTTP_PROTOCOL does not exist"
        driver.find_element(By.XPATH, self.HTTP_PROTOCOL).click()

        format_input = driver.find_element(By.XPATH, self.FORMAT_INPUT)
        assert format_input, f"FORMAT_INPUT does not exist"

        format_input.click()

        json_format_option = driver.find_element(By.XPATH, self.JSON_FORMAT)
        assert json_format_option, f"JSON_FORMAT does not exist"
        driver.find_element(By.XPATH, self.JSON_FORMAT).click()

        value_input = driver.find_element(By.XPATH, self.VALUE_INPUT)
        assert value_input, f"VALUE_INPUT does not exist"

        assert self.weather_ui_client.send_keys(
            value_input, city_for_search
        ), f"{city_for_search} is not writing in VALUE_INPUT "

        show_response_button = driver.find_element(By.XPATH, self.SHOW_RESPONSE_BUTTON)
        assert show_response_button, f"SHOW_RESPONSE_BUTTON does not exist"

        show_response_button = wait.until(
            EC.element_to_be_clickable((By.XPATH, self.SHOW_RESPONSE_BUTTON))
        )
        show_response_button.click()

        country_name_xpath_expression = self.COUNTRY_KEY
        country_name_element = wait.until(
            EC.element_to_be_clickable((By.XPATH, country_name_xpath_expression))
        )
        country_name = country_name_element.text

        assert (
            country_name.strip('"') == "country"
        ), f"Expected key with name  'country'"

        country_value_element = country_name_element.find_element(
            By.XPATH, "following-sibling::*[1]"
        )
        country_value = country_value_element.text

        assert (
            country_value.strip('"') == country_to_check
        ), f"Expected country to be {country_to_check}, but got {country_value}"

        self.weather_ui_client.quit_browser()
