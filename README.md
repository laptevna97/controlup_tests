
# Weather Automation Project
## Introduction

Weather Automation Project
This repository contains an automation project that performs API and UI tests using the WeatherAPI service. 

## Prerequisites

Before running the tests, ensure that you have the following prerequisites:<br />

Python installed (version 3.10.X)<br />
Pip package manager installed<br />
Git installed<br />
Chrome browser installed<br />
GitLab account for collaboration (optional)<br />

# Reference links

- [Selenium Documentation](https://www.selenium.dev/selenium/docs/api/py/index.html)

## Project Structure

The repository contains:<br />
- `api_tests` folder: Contains API Tests
- `ui_tests` folder: Contains UI Tests
- `automation_project` folder: Contains settings for both UI and API tests

## Setup

1. Clone the Repository:
```
git clone https://gitlab.com/laptevna97/controlup_tests.git
```

2.Navigate to the Project Directory:

```
cd controlup_tests
```
3.Create a Virtual Environment and activate it:

4.Install Dependencies:<br />
```
pip install -r requirements.txt
```
5.Set Environment Variables:

Create a .env file in the project root with the following content:

```
WEATHER_API_KEY=your_api_key_here
API_BASE_URL=http://api.weatherapi.com/v1
UI_BASE_URL=https://www.weatherapi.com/api-explorer.aspx
CITY_FOR_UI_AUTOMATION=city for UI test
COUNTRY_FOR_UI_AUTOMATION=country for UI test
```
Replace `your_api_key_here` with the API key obtained from WeatherAPI.

## Running Tests
### Running can be executed in one click
Execute `pytest` in the root directory of the repository.
Alternatively, you can run the tests using the `run_tests.py` script.
### API Test
Run the API test to retrieve the weather history in Paris three days ago and perform assertions:

```
pytest -k "test_api_weather_history"
```
### UI Test
Run the UI test to interact with the WeatherAPI UI, adding API Key, HTTP Protocol, JSON Format, and searching for a city:
```
pytest -k "test_ui_weather_search"
```
Note: If you encounter problems with running UI tests you will nead to edit `ui_client.py` in order to provide correct path to your chrome driver. Use https://chromedriver.chromium.org/downloads in order download new chromedriver for UI tests
## Contact information

For any questions  please contact laptevna97@gmail.com
